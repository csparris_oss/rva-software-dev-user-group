package com.kinsaleins.sdug.demo.controller;

import com.kinsaleins.sdug.demo.model.SdugHello;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloRvaSdugController {

    /**
     *
     * @return
     */
    @GetMapping("/hello-sdug")
    public String defaultCatchphrase(){
        return new SdugHello("Tim", "Explore the potential").getSdugStatement();
    }

    /**
     * Example: http://localhost:8080/hello-sdug-dynamic?name=Chris&catchPhrase=Cool%20Story%20Bro
     * Returns
     * <html>
     *     {"name":"Chris","catchPhrase":"Cool Story Bro","sdugStatement":"Hello Chris, Cool Story Bro!!!"}
     * </html>
     * @param name
     * @param catchPhrase
     * @return
     */
    @GetMapping("/hello-sdug-dynamic")
    public SdugHello dynamicCatchPhrase(@RequestParam(value = "name", defaultValue="Gaja") String name
            , @RequestParam(value = "catchPhrase", defaultValue="Explore the potential")String catchPhrase){
        return new SdugHello(name, catchPhrase);
    }


}
