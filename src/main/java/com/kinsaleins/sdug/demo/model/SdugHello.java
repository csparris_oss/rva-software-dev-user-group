package com.kinsaleins.sdug.demo.model;

public class SdugHello {

    private String name;
    private String catchPhrase;
    private String sdugStatement;

    public SdugHello(String name, String catchPhrase){
        this.name = name;
        this.catchPhrase = catchPhrase;
        this.sdugStatement = "Hello "+getName()+", "+getCatchPhrase()+"!!!";
    }

    public String getName() {
        return name;
    }

    public String getCatchPhrase() {
        return catchPhrase;
    }

    public String getSdugStatement() {
        return sdugStatement;
    }
}
